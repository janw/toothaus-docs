---
title: Terms of Service
translationKey: terms
---

## Terms of Use

This Mastodon instance is privately operated and does not provide any availability guarantees. Users are not entitled to access their accounts at all times. Downtimes (due to updates, maintenance windows, migrations, etc.) will be announced via the [Hausmeister](https://toot.haus/@hausmeister) account whenever possible.

The community rules and best practices of this instance are implemented in the [Code of Conduct](coc.md). Rule infractions, insults, discrimination and other inappropriate behavior will be sanctioned by silencing, or account deactivation at the sole discretion of the administrator.

## Privacy Policy

The following privacy policy provides information on how toot.haus processes and stores user data. It also details the legal rights users have towards the operator of the instance. These are derived from the European General Data Protection Regulation (GDPR), effective 2018-05-25.

### 1. Order Data Processing

The toot.haus instance uses the following external providers to provide its services:

* **Cloud services and servers from** \
    Hetzner Online GmbH \
    Industriestr. 25 \
    91710 Gunzenhausen, Germany
* **Cloud storage from** \
    Google Ireland Limited \
    Gordon House, Barrow Street \
    Dublin 4, Ireland

These are subject to their own data privacy regulations.

<!-- spell-checker:ignore Industriestr -->

### 2. Data processing when accessing the homepage, registration form and general use

When accessing toot.haus in order to register or also during general use, a connection is established to our servers used to operate this instance. In order to be able to display the page to the browser of the end device used, certain data is processed in accordance with the HTTP and TCP/IP protocols. This includes:

* the connection's IP address,
* the operating system of the PC, tablet or phone,
* the display resolution of the device,
* the browser and browser version in use, and
* the time when the website was accessed.

This technical data is processed so that the website can be transmitted to the end device used and can be correctly processed and displayed by your browser. Some of this data is stored in our server's logs each time you visit the site. We process this data for the purposes of server maintenance and security; IP addresses are deleted after 14 days at the latest.

The data processing is carried out in accordance with Art. 6 §1 b) GDPR to the extent necessary to enable the use of the homepage, the registration and general site access in the scope of the user relationship between us (the operator of the instance) and you (the operator of the Mastodon profile), as well as for the fulfillment of the obligation to take technical-organizational protective measures.

### 3. Data processing during registration

As part of the registration process we process basic account information: This includes your username, email address and password. You can enter additional profile information such as a display name or a biography and upload a profile picture or a header picture. The username, display name, biography, profile picture and header picture will then always be displayed publicly (possibly also to people who do not have their own Mastodon profile).

The data processing within the scope of the registration is carried out in accordance with Art. 6 §1 b) GDPR to the extent necessary to enable the login interface to be called up or the registration to be carried out within the scope of the user relationship.

### 4. Data processing during regular use of the Mastodon profile

#### Posts, followers and other public information

The list of people you follow is displayed publicly by default, the same applies to your followers. This can be deactivated in the settings. As soon as you send a message, the date and time is saved along with the information about which application you used to send your message. Messages can contain media content, such as images and videos. Public and unlisted posts are publicly available. Once you pin a post to your profile, it is also publicly available. Your posts are delivered to your followers, which in some cases means they are delivered to servers (other instances) and copies of them are stored there. Once you delete posts, this is also delivered to your followers, although we cannot guarantee that they will honour the deletion request. The act of sharing another post is always public by default.

#### Direct and "followers only" posts

All posts are stored and processed on the server. "Followers only" posts are delivered to your followers and to users you mention, direct messages (sometimes misleadingly called "private messages") only to users mentioned in them. However, direct messages are not end-to-end encrypted and can therefore - theoretically - also be viewed by us and, if applicable, by those responsible for the instance of the Mastodon profiles mentioned in the database or in messages. We can only ensure that unauthorized persons do not have access to the contributions stored on our instance, but other servers/instances could fail in this. For this reason, it may be useful to check the instances to which your followers belong. There is an option in the settings to manually accept or reject new followers. As a general rule, please do not share sensitive information on Mastodon, not even via direct messages.

#### IP addresses and other metadata

When you log in, Mastodon retains both the IP address from which you logged in and the name of your browser. All logged-in sessions are available for your review and revocation in the settings. Mastodon stores the last IP address used for up to 2 days. Furthermore, this information is used to protect the instance by way of rate limiting.

The data processing within the scope of the use is carried out in accordance with Art. 6 §1 b) GDPR to the extent necessary to enable you to use your Mastodon account within the scope of the user relationship.

### 5. Messages from third parties

We process personal data when users of third-party services that support ActivityPub interact with accounts on this instance. To enrich public profile pages with profile data, the following data is processed according to the requirements of the ActivityPub protocol:

* IP address of the third-party service
* Name of the user's software
* Display name, username und profile pictures
* Date and time of the interaction
* Profile data

The data processing is necessary to provide a federated Mastodon instance. It is therefore carried out in accordance with Article 6(1)(f) of the GDPR, with the exception of personal data that is not necessary, such as the display name and profile picture, the processing of which is based on Article 6(1)(a) of the GDPR. We store profile data from subscriptions to compatible third-party services until we receive a request to the contracy (unsubscribe, unlike, or unboost) via that service or directly from the user.

<!-- spell-checker:ignore unboost -->

### 6. Use of cookies

Mastodon uses cookies (small text files that make your browser uniquely distinguishable from others) to provide you with core and convenience features. These cookies allow the instance to recognize your browser and, if you have a registered account, to link it to your registered account so that you do not have to log in again and again. This cookie expires after one year or when you log out and is then usually removed automatically.

### 7. Data processing during email communication

If you contact the administration of the instance by email, your email, including the sender's address and content, will be processed and stored in order to handle the communication with you. This information will be stored for as long as is necessary to respond to the content of your request. Unless the specific nature of our communication requires it, old emails are generally deleted two years after receipt of the last response from you and contact details are deleted after three years.

The storage and use of the data contained in the emails as well as your contact data is carried out to the extent necessary due to our legitimate interest in handling communication with you, insofar as your interests do not conflict with this (Art. 6 §1 f) GDPR).

### 8. Your rights

You have the right that we, upon request,

* provide you with information about the personal data concerning you that we process (Art. 15 GDPR).
* correct inaccurate data (Art. 16 GDPR).
* delete your data if, for example, it is no longer necessary for the original purpose or if there is a dispute as to whether we are processing certain data lawfully (Art. 17 GDPR).

* restrict processing if the conditions of Art. 18 GDPR are met.
* provide the personal data concerning you that you have provided to us in a structured, commonly used and machine-readable format (Art. 20 GDPR).

In particular, you may object at any time, on grounds relating to a particular situation, to processing carried out on the basis of Article 6(1)(e) or (f) of the GDPR (Article 21 of the GDPR).

With regard to all of the above rights, a corresponding request sent by e-mail or in writing by post is sufficient to exercise your rights.

### 9. Responsible pertaining data protection law

&#74;&#97;&#110;&#32;&#87;&#105;&#108;&#108;&#104;&#97;&#117;&#115; \
&#76;&#105;&#115;&#116;&#101;&#114;&#32;&#75;&#105;&#114;&#99;&#104;&#119;&#101;&#103;&#32;&#57;&#52; \
&#51;&#48;&#49;&#55;&#55;&#32;&#72;&#97;&#110;&#110;&#111;&#118;&#101;&#114; \
E-Mail: &#97;&#100;&#109;&#105;&#110;&#64;&#116;&#111;&#111;&#116;&#46;&#104;&#97;&#117;&#115;

## 10. Supervisory authority

You have, in any case, the right to lodge a complaint with the regional data protection authority acting as the supervisory authority for me as the operator of this Mastodon instance.

Die Landesbeauftragte für den Datenschutz Niedersachsen \
Prinzenstraße 5 \
30159 Hannover \
Web: <https://lfd.niedersachsen.de> \
E-Mail: poststelle@lfd.niedersachsen.de

<!-- cspell:ignore Prinzenstraße -->

---

These terms of service are based on those of [chaos.social](https://chaos.social), which in turn are based on the (German) privacy notice of [legal.social](https://legal.social). It is licensed under [Creative Commons Attribution - ShareAlike 3.0 Germany License](http://creativecommons.org/licenses/by-sa/3.0/de/).
