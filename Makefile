GCS_BUCKET=gs://toothaus-storage-mastodon-docs-em9eugh0
BUCKET_SUBPATH=tmp
GSUTIL_OPTS=-o Credentials:gs_service_key_file=creds.json
LOGO_SOURCE="$(HOME)/Library/Mobile Documents/6LVTQB9699~com~seriflabs~affinitydesigner/Documents/toothaus"

.PHONY: deps
deps:
	pip install -U \
		mkdocs-material \
		mkdocs-git-revision-date-localized-plugin

.PHONY: build
build:
	hugo

.PHONY: update-logos
update-logos:
	cp \
		$(LOGO_SOURCE)/images/logo_transparent.svg \
		static/
	cp \
		$(LOGO_SOURCE)/icons/apple*.png \
		$(LOGO_SOURCE)/icons/favicon*.png \
		themes/toothaus/static/img/

.PHONY: publish
publish:
	gsutil $(GSUTIL_OPTS) -m rsync -d -r public/ "$(GCS_BUCKET)/$(BUCKET_SUBPATH)/"

.PHONY: unpublish
unpublish:
	test $(BUCKET_SUBPATH) != main || exit 1
	gsutil $(GSUTIL_OPTS) -m rm -r "$(GCS_BUCKET)/$(BUCKET_SUBPATH)" || true
