---
title: Dokumentation
---

Die technische Dokumentation für toot.haus ist nur auf Englisch verfügbar.

Du findest sie [hier]({{< ref path="docs" lang="en" >}}).
