---
title: Code of Conduct
translationKey: coc
---

The following rules and best practices provide a framework of social norms intended to foster an welcoming, inclusive, and generally positive atmosphere for the users of toot.haus. These rules are inherently incomplete and subject to change, yet should changes be necessary, they will be made in a transparent manner.

Users of toot.haus are expected to adhere to the rules and asked to follow the best practices. Infractions may lead to the deletion of toots, and repeated malicious unwillingness to follow them may warrant the silencing or suspension of accounts. These actions are taken at the sole discretion of the instance administrator. The administrator and moderator of this instance is [@jan](https://toot.haus/@jan). The server is privately owned and open to users voluntarily. It is not a public space, nor a so-called "free speech zone". Users wishing to join this community are expected to act without malice and in good faith. Doing otherwise may lead to removal from the service, regardless of whether any of the rules outlined below were in fact violated or not.

Users are asked to report content or behavior that they consider violations of the rules or that otherwise make them feel uncomfortable. Such reports will always be kept confidential.

## Rules

* We do not tolerate discriminatory behavior, and content promoting or advocating for the oppression of members of marginalized groups. Characterizations of these groups may include (but are not limited to) ethnicity, gender and sexual identity or expression; physical characteristics such as age, disability or illness; nationality, residency, or citizen status; wealth or education; religious affiliation, agnosticism or atheism.
* We do not tolerate threatening behavior, stalking, or [doxxing](https://en.wikipedia.org/wiki/Doxxing).
* We do not tolerate harassment, including brigading, dogpiling, or any other form of contact with a user who has stated that they do not wish to be contacted.
* We do not tolerate mobbing, including name-calling, intentional misgendering or deadnaming.
* We do not tolerate violent nationalist propaganda, Nazi symbolism or promoting the ideology of National Socialism.
* We do not tolerate conspiracy narratives or other reactionary myths supporting or leading to the aforementioned (and/or similar) behavior.
* Actions intended to damage this instance or its performance may lead to immediate account suspension.
* Sexual content and content depicting violence must use content warnings.
* Content that is illegal in Germany will be deleted and may lead to immediate account suspension.

## Best Practices

* In general, use the tools provided to foster a considerate and accessible atmosphere. This includes the liberal use of content warnings (especially on potentially disturbing or controversial topics), and alt-text captioning of media files to aide accessibility.
* When possible, provide credit for creative works in your posts that are not your own.
* Uninvited comments about another user's personal choices, lifestyle or family are strongly discouraged and may be considered harassment. Inappropriate sexual attention, comments about appearance and implication of physical contact will not be tolerated toward any non-consenting user.
* If you post advertisements, use a content warning. Advertisements should not be excessive or automated.
* Bots may only interact with a user when they're invited by that user to do so.
* Automated posts and high-frequency posts should be created as "unlisted" (visible to everybody, but not appearing on the local timeline) to keep the local timeline of toot.haus a place of community dialogue and human interaction. This extends to bots, feed posters, crossposters, and all other automated posts.
* In discussions, please remain civil. Do not insult the people you're talking to. Note that irony, sarcasm, or similar modes of language don't translate well to written language and tend to escalate discussions or misunderstandings.
* I, [@jan](https://toot.haus/@jan), am maintaining this instance in my spare time, using resources that I pay for with my own money. Please be considerate and avoid breaking things.
