---
title: Impressum
translationKey: imprint
---


## Angaben gemäß § 5 TMG

&#74;&#97;&#110;&#32;&#87;&#105;&#108;&#108;&#104;&#97;&#117;&#115; \
&#76;&#105;&#115;&#116;&#101;&#114;&#32;&#75;&#105;&#114;&#99;&#104;&#119;&#101;&#103;&#32;&#57;&#52; \
&#51;&#48;&#49;&#55;&#55;&#32;&#72;&#97;&#110;&#110;&#111;&#118;&#101;&#114; \
E-Mail: &#97;&#100;&#109;&#105;&#110;&#64;&#116;&#111;&#111;&#116;&#46;&#104;&#97;&#117;&#115;

## Verantwortlich für den Inhalt nach MStV

&#74;&#97;&#110;&#32;&#87;&#105;&#108;&#108;&#104;&#97;&#117;&#115; \
&#76;&#105;&#115;&#116;&#101;&#114;&#32;&#75;&#105;&#114;&#99;&#104;&#119;&#101;&#103;&#32;&#57;&#52; \
&#51;&#48;&#49;&#55;&#55;&#32;&#72;&#97;&#110;&#110;&#111;&#118;&#101;&#114;

## Haftung für Inhalte

Als Anbieter bin ich gemäß § 7 Abs. 1 TMG für eigene Inhalte auf dieser Mastodon-Instanz nach den allgemeinen Gesetzen verantwortlich. Nach §§ 8 bis 10 TMG bin ich als Diensteanbieter jedoch nicht verpflichtet, übermittelte oder gespeicherte fremde Informationen (konkret z.B. Posts von Nutzer:innen) zu überwachen oder nach Umständen zu forschen, die auf eine rechtswidrige Tätigkeit hinweisen. Verpflichtungen zur Entfernung oder Sperrung der Nutzung von Informationen nach den allgemeinen Gesetzen bleiben hiervon unberührt. Eine diesbezügliche Haftung ist jedoch erst ab dem Zeitpunkt der Kenntnis einer konkreten Rechtsverletzung möglich. Bei Bekanntwerden von entsprechenden Rechtsverletzungen werden ich umgehend entsprechende Schritte ergreifen.

## Haftung für Links

Diese Instanz enthält Links zu externen Webseiten, auf deren Inhalte ich keinen Einfluss habe. Deshalb kann ich für diese fremden Inhalte auch keine Gewähr übernehmen. Für die verlinkten Inhalte ist stets die jeweilige Anbieterin oder Betreiberin der Seiten verantwortlich. Eine permanente inhaltliche Kontrolle der verlinkten Seiten ist ohne konkrete Anhaltspunkte einer Rechtsverletzung nicht zumutbar. Bei Bekanntwerden von Rechtsverletzungen werde ich umgehend die gebotenen Schritte ergreifen.

## Urheberrecht

Die hier durch die Mastodon-User oder mich erstellten Inhalte unterliegen dem Urheberrecht. Die Vervielfältigung, Bearbeitung, Verbreitung und jede Art der Verwertung außerhalb der Grenzen des Urheberrechtes bedürfen in der Regel der schriftlichen Zustimmung der jeweiligen Autorin bzw. Erstellerin. Downloads und Kopien sind grundsätzlich nur für den privaten, nicht kommerziellen Gebrauch gestattet. Sollte eine Urheberrechtsverletzung auffallen, bitte ich um einen entsprechenden Hinweis. Bei Bekanntwerden von Rechtsverletzungen werde ich hinsichtlich derartiger Inhalte umgehend die notwendigen Schritte ergreifen.

----

Das Impressum basiert auf dem von [legal.social](https://legal.social) und ist lizenziert unter einer [Creative Commons Namensnennung - Weitergabe unter gleichen Bedingungen 3.0 Deutschland Lizenz](http://creativecommons.org/licenses/by-sa/3.0/de/).
