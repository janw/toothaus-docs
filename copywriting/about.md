<!-- markdownlint-disable MD041 -->

Toot.haus was created in a desire to build a positive and inclusive community of nerdy and nerd-adjacent people, who feel like they "don't fit in" in whatever way on other instances. Toot.haus is open to anyone willing to follow the rules linked. We provide a safe house if you need one. The primary languages on this instance are German and English.

Please find our rules in the [Code of Conduct](https://about.toot.haus/coc) (and its German [Verhaltenskodex](https://about.toot.haus/de/verhaltenskodex)  counterpart), as well as additional documentation on [about.toot.haus](https://about.toot.haus)
