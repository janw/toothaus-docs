---
title: Verhaltenskodex
translationKey: coc
---

Die folgenden Regeln und Handlungsempfehlungen schaffen einen Rahmen, um eine einladende, offene und grundsätzlich positive Atmosphäre für die Nutzer:innen von toot.haus zu schaffen. Die Regeln erheben prinzipbedingt keinen Anspruch auf Vollständigkeit und können sich im Laufe der Zeit ändern. Sollte dies der Fall sein, werden die Änderungen jedoch transparent kommuniziert.

Nutzer:innen von toothaus sind dazu angehalten die folgenden Regeln einzuhalten und sich im Sinne der Handlungsempfehlungen zu verhalten. Der Server wird als privates Hobby betrieben und wird Nutzer:innen freiwillig zu Verfügung gestellt. Es ist kein öffentlicher Raum und gibt keine Garantie auf absolute Redefreiheit. Mit der Registrierung wird von Nutzer:innen erwartet, dass sie ohne böse Absichten handeln. Verstöße gegen die Regeln die Löschung von Toots nach sich ziehen und die wiederholte (insbesondere absichtliche) Missachtung kann eine Stummschaltung, Sperrung oder Löschung des Kontos zur Folge haben. Derartige Schritte liegen im alleinigen Ermessen des Administrators, [@jan](https://toot.haus/@jan).

<!-- spell-checker:ignore Stummschaltung -->

Nutzer:innen werden darum gebeten, Inhalte oder Verhalten zu melden, dass ihrer Ansicht nach gegen die Regel verstößt oder anderweitig Unbehagen bei ihnen auslöst. Solche Meldungen werden immer vollkommen vertraulich behandelt.

## Regeln

* Wir dulden kein diskriminierendes Verhalten oder Inhalte, die die Unterdrückung marginalisierter Gruppen unterstützt oder verteidigt. Zur Charakterisierung solcher Gruppen zählen z.B. (aber nicht ausschließlich) Ethnie, Geschlechts- und sexuelle Identität oder Ausdruck derer; körperliche Merkmale wie Alter, Behinderung oder Krankheit; Nationalität, Wohnsitz oder Staatsangehörigkeit; Vermögen oder Bildungsstand; religiöse Zugehörigkeit, Agnostizismus oder Atheismus.
* Wir dulden kein Drohverhalten, Stalking oder [Doxxing](https://de.wikipedia.org/wiki/Doxxing).
* Wir dulden keine Belästigung, insbesondere in koordinierter Form als sog. Brigading oder Dogpiling. Dies umfasst auch die Kontaktaufnahme von anderen Personen, die ausdrücklich nicht kontaktiert werden möchten.
* Wir dulden kein Mobbing, Beschimpfungen, absichtliches Misgendering oder [Deadnaming](https://de.wikipedia.org/wiki/Deadnaming).
* Wir dulden keine nationalistische Propaganda, die Verwendung von Nazi-Symbolen oder die Verherrlichung nationalsozialistischer Ideologie.
* Wir dulden keine Verschwörungserzählungen und ähnliche reaktionäre Mythen, die o.g. Verhaltensweisen unterstützen.
* Wir dulden kein Verhalten, das die Beeinträchtigung dieser Instanz oder ihrer Leistungsfähigkeit zur Absicht hat.
* Sexuelle Inhalte und Gewaltdarstellungen müssen mit einer Inhaltswarnung versehen sein.
* Inhalte, die nach deutschem Recht illegal sind, werden entfernt.

<!-- spell-checker:ignore Verschwörungserzählungen -->

## Handlungsempfehlungen

* Bitte nutze die Werkzeuge von Mastodon, um eine rücksichtsvolles und barrierereduziertes Miteinander zu gewährleisten. Dazu gehören in erster Linie die großzügige Anwendung von Inhaltswarnungen (Content-Warnings; insbesondere bei potenziell verstörenden oder kontroversen Themen) und das Verfassen von Bildunterschriften für Medienanhänge.
* Wann immer möglich, referenziere die Quellen von künstlerischen Erzeugnissen, die nicht deine eigenen sind.
* Von ungefragten Kommentaren zu den persönlichen Entscheidungen anderer Nutzer:innen, ihrem Lifestyle oder ihrer Familie raten wir entschieden ab. Sie können auch als Form der Belästigung verstanden werden. Sexualisierte Aufmerksamkeit, Bemerkungen zum Erscheinungsbild oder Andeutungen auf Körperkontakt bedürfen der vorherigen Einwilligung der betreffenden Person.
* Wenn du für etwas wirbst, nutze auch hier eine Inhaltswarnung. Werbetoots sind nur vereinzelt zulässig und dürfen nicht automatisiert sein.
* Bots dürfen mit Nutzer:innen nur in Kontakt treten, wenn diese sich dazu bereiterklärt haben.
* Automatisierte Beitrage sollten als "Nicht gelistet" (für alle sichtbar aber nicht in der lokalen Timeline erscheinend) erstellt werden, damit die lokale Timeline ein Ort für Dialog und Interaktionen zwischen Menschen bleibt. Dies trifft auch auf Bots, Feed-Poster und Twitter-Crossposter zu.
* Bleibe in Diskussionen höflich. Beleidige niemanden und beachte, dass Ironie, Sarkasmus und ähnliche Sprachmodi in der Schriftform häufig nicht zum Ausdruck kommen und dazu neigen, zur Eskalation und Missverständnissen beizutragen.
* Ich, [@jan](https://toot.haus/@jan), betreibe diese Instanz in meiner Freizeit und auf Infrastruktur für die ich mit meinem eigenen Geld bezahle. Bitte nimm darauf Rücksicht.

<!-- spell-checker:ignore barrierereduziertes  -->
<!-- spell-checker:ignore Werbetoots -->
