---
title: Willkommen im Toothaus
layout: single
hideLastMod: true
---

Toot.haus is eine neue und kleine Mastodon-Instanz. Sie wurde gegründet, um eine offene und positive Community von nerdigen und nerd-nahen Menschen aufzubauen, die das Gefühl haben, auf anderen Instanzen irgendwie nicht richtig dazuzugehören. Toot.haus steht allen offen, die unseren [Verhaltenskodex]({{< ref "verhaltenskodex" >}}) beachten.

Wir bieten dir ein sicheres Haus, wenn du eines brauchst.

Die Hauptsprachen dieser Instanz sind Deutsch und [Englisch](/).

<!-- spell-checker:ignore nerdigen -->
