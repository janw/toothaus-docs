---
title: Documentation

---

## Status page

A status page with basic uptime statistics is available at [status.toot.haus](https://status.toot.haus/).

I'm planning to add a more detailed infrastructure dashboard soon.

## Mastodon Version

Toot.haus runs a near-vanilla version of Mastodon. You can find the fork in this [repository](https://gitlab.com/janw/mastodon) including a [comparison to upstream](https://gitlab.com/janw/mastodon/-/compare/main...toothaus).

## Blocked Instances

Toot.haus does block certain instances that are considered harmful to the ideals of the Fediverse and this instance. The blocked servers include those used exclusively for Twitter scraping/crossposting, those known for spamming, and those engaging in discrimination, harassment, or other forms of hate speech, as well as so-called "free speech zones" and others exhibiting malicious behavior.

Although publicizing a list of the blocked instances here would be preferred in the interest of transparency, doing so would give undeserved attention to those instances. Therefore please just be assured that toot.haus only blocks the same instances that most other reasonable instances block, and that we use common sense and a diligent manual review process when blocking other malicious instances.

## Privacy Enhancements

As stated, Toot.haus runs the official open-source Mastodon software. Though the particular version we run adds a few modifications and configuration settings intended to improve the privacy of our users. It is our belief that running a public web service implies being a good steward of your user's data. This means ensuring confidentiality of the collected data while applying the principle of data economy to reduce the amount of data that is collected in the first place.

To follow this statement we have implemented the following additional measures that go beyond the default behavior of Mastodon itself.

### No access logs

Many webservers emit so-called access logs by default. This means that all requests made to the service will be stored in log files for some amount of time and can be inspected by the operator. These logs often contain the exact URL visited, the device used to make the request, and most notably the user's IP address. At toot.haus, we do not log user requests and have disabled access logs entirely.

### Two-day IP address retention

Another default behavior of webservers is to expose the user's IP address to the running application when a request is being made. Mastodon makes IP addresses available for moderation and administration purposes, and keeps track of

* the IP address used to sign up to an instance,
* the IP addresses used to make log in attempts to an account, and
* the IP addresses used for recurring (session) access after successful login.

By default all of these are stored for up to *one year*. At toot.haus we significantly shorten the duration of how long IP addresses are kept for so that they are removed after just two days in the database.

## Infrastructure

Toot.haus is hosted on servers at [Hetzner Cloud](https://www.hetzner.com/cloud) in datacenters in Nuremberg, Germany. The instance is operated in a small Kubernetes cluster, currently consisting of 3 nodes. All components are set up in highly available configurations, except for the Postgres database which runs on a separate machine (it will be upgraded to an HA setup in the upcoming months.  Backups are created daily and sent off-site to a storage box in Helsinki, Finland. There we keep 7 daily, 4 weekly, 6 monthly and 10 yearly snapshots.

Media files (attachments, avatars, etc.) are being *stored* in Google Cloud Storage (in the `europe-west3` region in Frankfurt, Germany) but *retrieved* by proxying through the cluster's load balancer, so that requests appear to originate from toot.haus itself. No personally identifiable information (PII) is passed to Google servers as stated under [No IP Addresses](#no-ip-addresses) above.
