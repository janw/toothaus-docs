<!-- markdownlint-disable MD041 -->
You can find all relevant policies here:

English: [Terms of Service, Privacy Policy](https://about.toot.haus/tos)

German: [Allgemeine Geschäftsbedingungen, Datenschutzhinweise](https://about.toot.haus/de/agb), [Impressum](https://about.toot.haus/de/impressum)
