---
title: Allgemeine Geschäftsbedingungen
translationKey: terms
---

## Nutzungsbedingungen

Diese Mastodon-Instanz wird als privates Hobby ohne Garantie für Verfügbarkeit betrieben. Es besteht kein Anspruch auf jederzeitigen funktionierenden Zugang zum Konto. Downtimes (Updates, Wartung, Migration etc) werden - soweit möglich - vorher über den [Hausmeister](https://toot.haus/@hausmeister)-Account angekündigt.

Die Regeln für das Miteinander der Instanz sind im [Verhaltenskodex](/de/verhaltenskodex) geregelt. Bei Nichteinhaltung dieser Regeln, sowie beleidigendem, diskriminierendem oder anderweitig unangebrachtem Verhalten behalten wir uns vor, Accounts – auch ohne Ankündigung – zu sanktionieren oder zu deaktivieren.

## Datenschutzhinweise

Diese Datenschutzhinweise klären darüber auf, welche Informationen bei der Nutzung der Mastodon-Instanz toot.haus verarbeitet werden und welche Rechte gegenüber mir, dem Betreiber der Instanz, bestehen. Alle hier dargestellten Ausführungen und die Aufklärung über die Rechte richten sich nach der seit dem 25.05.2018 geltenden europäischen Datenschutz-Grundverordnung (DSGVO).

### 1. Auftragsdatenverarbeitung

Für das Hosting nutzt toot.haus

* **Cloud-Dienste und Server der** \
    Hetzner Online GmbH \
    Industriestr. 25 \
    91710 Gunzenhausen, Deutschland
* **Cloud-Speicher der** \
    Google Ireland Limited \
    Gordon House, Barrow Street \
    Dublin 4, Ireland

Diese unterliegen ihren eigenen Datenschutzbestimmungen.

<!-- spell-checker:ignore Industriestr -->

### 2. Datenverarbeitung bei Aufruf der Startseite, Registrierungsmaske und Nutzung im Allgemeinen

Sobald die Startseite von toot.haus aufgerufen wird, um sich zu registrieren oder auch während der Nutzung im Allgemeinen, wird eine Verbindung zu den Servern aufgebaut, auf denen diese Instanz betrieben wird. Um dem Browser des genutzten Endgeräts die Seite anzeigen zu können, werden dabei entsprechend der HTTP und TCP/IP Protokolle gewisse Daten verarbeitet. Dazu gehören:

* die IP-Adresse des Internetanschlusses,
* die Betriebssystemversion des PCs, Tablets oder Smartphones,
* die Displayauflösung des Geräts,
* der Standort des Geräts,
* der genutzte Internetbrowser sowie
* der Zeitpunkt des Zugriffs auf die Webseite.

Diese technischen Daten werden verarbeitet, damit diese Webseite von dem Webserver an das genutzte Endgerät übermittelt und von Ihrem Browser richtig verarbeitet und angezeigt werden kann. Nach jedem Seitenbesuch werden einige dieser Daten in den Logs der Server gespeichert. Diese Daten verarbeiten wir zu Zwecken der Wartung und Sicherheit der Server. IP-Adressen werden dabei nach spätestens 14 Tagen gelöscht; in der Regel früher. Die Server dieser Instanz wird von einem professionellen Hoster mit Servern in der Europäischen Union betrieben. Er unterliegt als Auftragsverarbeiter meinen Anweisungen und hat sich vertraglich zur Einhaltung technischer und organisatorischer Sicherungsmaßnahmen verpflichtet.

Die Datenverarbeitung erfolgt dabei gemäß Art. 6 Abs. 1 Buchst. b) DSGVO im erforderlichen Umfang, um den Aufruf der Startseite sowie die Registrierung im Rahmen des Nutzungsverhältnisses zwischen mir (dem Betreiber der Instanz) und Ihnen (der Betreiberin des Mastodon-Profils) zu ermöglichen sowie zur Erfüllung der Verpflichtung zur Ergreifung technisch-organisatorischer Schutzmaßnahmen.

### 3. Datenverarbeitung bei der Registrierung

Im Rahmen der Registrierung werden grundlegende Kontoinformationen verarbeitet. Dazu zählen Benutzername, E-Mail-Adresse und Passwort. Nutzer:innen können auch zusätzliche Profilinformationen, wie einen Anzeigenamen oder eine Biografie eingeben, und ein Profilbild oder ein Headerbild hochladen. Der Benutzername, der Anzeigename, die Biografie, das Profilbild und das Headerbild werden dann immer öffentlich angezeigt; ggf. solchen auch Personen, die kein eigenes Mastodon-Profil haben.

<!-- spell-checker:ignore Headerbild -->

Die Datenverarbeitung im Rahmen der Registrierung erfolgt dabei gemäß Art. 6 Abs. 1 Buchst. b) DSGVO im erforderlichen Umfang, um den Aufruf der Login-Oberfläche bzw. die Durchführung der Registrierung im Rahmen des Nutzungsverhältnisses zu ermöglichen.

### 4. Datenverarbeitung bei der Nutzung des Mastodon-Profils

#### Beiträge, Folge- und andere öffentliche Informationen

 Die Liste der Leute, denen Sie folgen, wird öffentlich gezeigt, das gleiche gilt für Ihre Folgenden (Follower). Sobald Sie eine Nachricht übermitteln, wird das Datum und die Uhrzeit gemeinsam mit der Information, welche Anwendung Sie dafür verwendet haben, gespeichert. Nachrichten können Medienanhänge enthalten, etwa Bilder und Videos. Öffentliche und ungelistete Beiträge sind öffentlich verfügbar. Sobald Sie einen Beitrag auf Ihrem Profil anpinnen, ist dieser ebenfalls öffentlich verfügbar. Ihre Beiträge werden an Ihre Folgenden ausgeliefert, was in manchen Fällen bedeutet, dass sie an andere Server (andere Instanzen) ausgeliefert werden und dort Kopien gespeichert werden. Sobald Sie Beiträge löschen, wird dies ebenso an Ihre Follower ausgeliefert. Die Handlungen des Teilens eines anderen Beitrages sind immer öffentlich.

<!-- spell-checker:ignore ungelistete -->

#### Direkte und "Nur Folgende"-Beiträge

 Alle Beiträge werden auf dem Server gespeichert und verarbeitet. "Nur Folgende"-Beiträge werden an Ihre Folgenden und an Benutzer:innen, die Sie erwähnen, ausgeliefert, direkte Beiträge (manchmal missverständlich als "Direktnachrichten" bezeichnet) nur an in ihnen erwähnte Benutzer:innen. Direkte Beiträge sind allerdings nicht Ende-zu-Ende verschlüsselt und daher – theoretisch – auch durch mich sowie ggf. die Verantwortlichen der Instanzen der erwähnten Mastodon-Profile einsehbar. Ich kann nur sicherstellen, dass Unbefugte keinen Zugang zu den auf meiner Instanz gespeicherten Beiträge haben, jedoch könnten andere Server/Instanzen daran scheitern. Deswegen kann es sinnvoll sein, die Instanzen, zu denen Ihre Folgenden gehören, zu überprüfen. Sie können auch eine Option in den Einstellungen umschalten, um neue Folgende manuell anzunehmen oder abzuweisen. Bitte beachten Sie, dass die Betreibende des Servers und jedes empfangenden Servers solche Nachrichten anschauen könnten und dass Empfänger:innen von diesen eine Bildschirmkopie erstellen, sie kopieren oder anderweitig weiterverteilen könnten. Teilen Sie daher keine sensiblen Informationen über Mastodon, auch nicht über direkte Beiträge.

#### IP-Adressen und andere Metadaten

Sobald Sie sich anmelden, erfasst Mastodon sowohl die IP-Adresse, von der aus Sie sich anmelden, als auch den Namen Ihrer Browseranwendung. Alle angemeldeten Sitzungen (Sessions) sind für Ihre Überprüfung und Widerruf in den Einstellungen verfügbar. Die letzte verwendete IP-Adresse speichert Mastodon bis zu 2 Tage lang. Sie wird auch zum Schutz dieser Instanz durch Durchsatzratenbegrenzung verwendet.

Die Datenverarbeitung im Rahmen der Nutzung erfolgt dabei gemäß Art. 6 Abs. 1 Buchst. b) DSGVO im erforderlichen Umfang, um Ihnen die Nutzung Ihres Mastodon Accounts im Rahmen des Nutzungsverhältnisses zu ermöglichen.

<!-- spell-checker:ignore Durchsatzratenbegrenzung -->

### 5. Beiträge von Diensten Dritter

Wir verarbeitet personenbezogene Daten, wenn Nutzer*innen von Drittdiensten mit ActivityPub-Unterstützung mit Konten auf dieser Instanz interagieren. Um öffentliche Profilseiten mit Profildaten anzureichern, werden die folgenden Daten gemäß den Anforderungen des ActivityPub-Protokolls verarbeitet:

* IP-Adresse des Drittdienstes
* Name der Endgerätesoftware des Nutzers
* Anzeigename, Kontoname und Profilbild
* Aktuelles Datum und Uhrzeit
* Profildaten

Die Datenverarbeitung ist notwendig, um eine föderierte Mastodon-Instanz bereitzustellen. Sie erfolgt daher gemäß Artikel 6 Abs. 1 Buchst. f) DSGVO, mit Ausnahme von personenbezogenen Daten, die nicht erforderlich sind, wie der Anzeigename und das Profilbild, deren Verarbeitung auf Artikel 6 Abs. 1 Buchst. a) DSGVO beruht. Wir speichern Profildaten aus Abonnements von kompatiblen Drittdiensten, bis es über diesen Dienst oder direkt vom Nutzer eine Aufforderung zur Löschung oder zum Widerspruch (unsubscribe, unlike, unboost) erhalten.

<!-- spell-checker:ignore unboost -->

### 6. Nutzung von Cookies

Mastodon verwendet Cookies (kleine Textdateien, die Ihren Browser eindeutig unterscheidbar von anderen machen), um Ihnen Kern- und Komfortfunktionen bereitzustellen. Diese Cookies ermöglichen es der Instanz u.a. Ihren Browser wiederzuerkennen und, sofern Sie ein registriertes Konto haben, diesen mit Ihrem registrierten Konto zu verknüpfen.

### 7. Datenverarbeitung bei der E-Mail-Kommunikation

Wenn Sie sich per E-Mail an die Administration der Instanz wenden, wird Ihre E-Mail samt Absenderadresse und Inhalt verarbeitet und gespeichert, um die Kommunikation mit Ihnen abzuwickeln. Diese Informationen werden so lange gespeichert, wie es erforderlich ist, um dem Inhalt Ihres Anliegens gerecht zu werden. Sofern nicht die besondere Natur unserer Kommunikation es erfordert, werden die alten E-Mails in der Regel ein Jahr nach dem Empfang der letzten Rückmeldung von Ihnen und die Kontaktdaten nach zwei Jahren gelöscht.

Die Speicherung und Nutzung der in den E-Mails enthaltenen Daten sowie Ihrer Kontaktdaten erfolgt im erforderlichen Umfang aufgrund meines berechtigten Interesses an der Abwicklung der Kommunikation mit Ihnen, soweit Ihre Interessen dem nicht entgegenstehen (Art. 6 Abs. 1 Buchst. f) DSGVO).

### 8. Ihre Rechte

Sie haben das Recht, zu verlangen,

* dass ich Auskunft über die Sie betreffenden und von mir hier verarbeiteten personenbezogenen Daten gebe (Art. 15 DSGVO),
* dass ich unrichtige Daten berichtige (Art. 16 DSGVO),
* dass Ihre Daten gelöscht werden, wenn diese z.B. nicht mehr erforderlich für den ursprünglichen Zweck sind oder Streit darüber besteht, ob ich bestimmte Daten rechtmäßig verarbeite (Art. 17 DSGVO),
* dass die Verarbeitung Ihrer Daten eingeschränkt wird, wenn die Voraussetzungen des Art. 18 DSGVO vorliegen.
* die Sie betreffenden personenbezogenen Daten, die Sie mir bereitgestellt haben, in einem strukturierten, gängigen und maschinenlesbaren Format zu erhalten (Art. 20 DSGVO).

Insbesondere können Sie aus Gründen, die sich aus einer besonderen Situation ergeben, jederzeit gegen die Verarbeitung, die aufgrund von Art. 6 Abs. 1 Buchst. e) oder f) DSGVO erfolgt, Widerspruch einlegen (Art. 21 DSGVO). Bezüglich aller genannten Rechte gilt dabei: Ein entsprechendes per E-Mail (&#97;&#100;&#109;&#105;&#110;&#64;&#116;&#111;&#111;&#116;&#46;&#104;&#97;&#117;&#115;) oder schriftlich per Post (Adresse im Impressum) übermitteltes Verlangen genügt, um Ihre Rechte in Anspruch zu nehmen.

### 9. Verantwortlicher im Sinne des Datenschutzrechts

&#74;&#97;&#110;&#32;&#87;&#105;&#108;&#108;&#104;&#97;&#117;&#115; \
&#76;&#105;&#115;&#116;&#101;&#114;&#32;&#75;&#105;&#114;&#99;&#104;&#119;&#101;&#103;&#32;&#57;&#52; \
&#51;&#48;&#49;&#55;&#55;&#32;&#72;&#97;&#110;&#110;&#111;&#118;&#101;&#114; \
E-Mail: &#97;&#100;&#109;&#105;&#110;&#64;&#116;&#111;&#111;&#116;&#46;&#104;&#97;&#117;&#115;

### 10. Zuständige Datenschutzaufsicht

Sollten Sie der Meinung sein, dass ich das geltende Datenschutzrecht verletze, dann können Sie sich jederzeit an eine Datenschutzaufsichtsbehörde wenden. Für mich im Kontext des Betriebs dieser Mastodon-Instanz zuständig ist

Die Landesbeauftragte für den Datenschutz Niedersachsen \
Prinzenstraße 5 \
30159 Hannover \
Web: <https://lfd.niedersachsen.de> \
E-Mail: poststelle@lfd.niedersachsen.de

<!-- cspell:ignore Prinzenstraße -->

---

Das Datenschutzhinweise basieren auf denen von [legal.social](https://legal.social) und sind lizenziert unter einer [Creative Commons Namensnennung - Weitergabe unter gleichen Bedingungen 3.0 Deutschland Lizenz](http://creativecommons.org/licenses/by-sa/3.0/de/).
