---
title: Welcome to toot.haus
layout: single
hideLastMod: true
---

Toot.haus is a young and small Mastodon instance. It was created in a desire to build a positive and inclusive community of nerdy and nerd-adjacent people, who feel like they "don't fit in" in whatever way on other instances. Toot.haus is open to anyone willing to follow the [code of conduct](coc.md).

We provide a safe house if you need one.

The primary languages on this instance are English and [German](/de/).
